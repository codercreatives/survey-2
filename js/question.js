$(document).ready(function () {
    $("a#nextButton").click(function (e) {
        e.preventDefault();
        $("#congratulations").addClass("animate__animated animate__fadeOutLeft")
        $("#congratulations").delay(500).queue(function () {
            $(this).addClass("hide")
        })
        $("#startBox").addClass("hide");
        $("#questionBox").delay(300).queue(function(){
            $(this).removeClass("hide")
            $(this).addClass("animate__animated animate__fadeInRight")
        })
    });

    // Question 1
    $(".question-1").click(function(e){
        e.preventDefault();
        $("#question1").hide();
        $("#question2").removeClass("hide")
    })

    // Question 2
    $(".question-2").click(function(e){
        e.preventDefault();
        $("#question2").hide();
        $("#question3").removeClass("hide")
    })

    // Question 3
    $(".question-3").click(function(e){
        e.preventDefault();
        $("#question3").hide();
        $("#question4").removeClass("hide")
    })

    // Question 4
    $(".question-4").click(function(e){
        e.preventDefault();
        $("#question4").hide();
        $("#question5").removeClass("hide")
    })

    // Question 5
    $(".question-5").click(function(e){
        e.preventDefault();
        $("#question5").hide();
        $("#question6").removeClass("hide")
    })

    // Question 6 
    $(".question-6").click(function(e){
        e.preventDefault();
        $("#question6").hide();
        $("#question7").removeClass("hide")
    })

    $(".question-7").click(function(e){
        e.preventDefault()
        $("#question7").hide()
        $("#progress-complete").removeClass("hide")
       
        var timerId, percent

        percent = 0;

        timerId = setInterval(function(){
            percent +=5
            console.log("percent: " + percent)
            $("#progress-bar").css("width", percent + "%")
            $("#progress-bar").text( percent + "%")

            if(percent === 35){
                $("#status-1").removeClass("hide")
                $("#status-1").addClass("animate__animated animate__zoomIn")
            }
            if(percent === 65){
                $("#status-2").removeClass("hide")
                $("#status-2").addClass("animate__animated animate__zoomIn")
            }
            if(percent === 95){
                $("#status-3").removeClass("hide")
                $("#status-3").addClass("animate__animated animate__zoomIn")
            }
            if(percent >=100){
                clearInterval(timerId)
                $("#progress-complete").addClass("animate__animated animate__fadeOut")
                 $("#verification-complete").addClass("animate__animated animate__fadeIn")
                 $("#verification-complete").removeClass("hide")

            }

            setTimeout(function(){
                $("#status-1").hide(200)
                $("#status-2").hide(400)
                $("#status-3").hide(600)
                $("#offer-wall").removeClass("hide")
                $("#offer-wall").addClass("animate__animated animate__fadeIn")
            },6000)
        },200)        
        
    })

    $(".claimBtn").click(function(e){
        e.preventDefault();
        
    })
    


})